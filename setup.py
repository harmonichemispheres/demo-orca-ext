from setuptools import setup, find_packages

setup(
    name='demo-orca-ext',
    version='1.0.0',
    description='demo-orca-ext is an Orca RPA Extension',
    url='',
    author='robby',
    author_email='',

    # the source code folders
    packages=find_packages(),

    python_requires='>=3.7',
    classifiers=[
        "Development Status :: 1 - Alpha",
        "Programming Language :: Python",
        "Operating System :: OS Independent",
        "Topic :: Robotic Process Automation",
        "Topic :: Automation"
    ]
)
