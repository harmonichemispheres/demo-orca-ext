# demo-orca-ext
`demo-orca-ext` is an orca extension


## Install

**Install Locally**
```
$ pip install -e .
```

## Usage
add the extension to your orca bot configuration.
then simply use the extension resources in your workflow.

```yaml
# in yaml
extensions:
-   name: demo-orca-ext

workflow:
    node: HelloWorld
    msg: This is a Node from an Orca Extension!
```

```python
# in python
from orca import Bot

Bot(
    title='demo',
    extensions=[
        {'name': 'demo-orca-ext'}
    ],
    workflow={
        'node': 'HelloWorld',
        'msg': 'This is a Node from an Orca Extension!'
    }
)
```

