
from orca import (
    Node, Port, MetaData, NodeError
)

class HelloWorld(Node):
    """
    :param value: print text to the console
    :type value: str
    """

    node_type = 'action'
    
    md = MetaData(
        name="DemoNode",
        desc="This node is a template"
    )

    msg = Port(
        direction="in",
        datatype=str,
        desc="string to print",
        is_required=True
        )


    def run(self):
        print(self.msg.val)