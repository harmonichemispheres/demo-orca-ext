
from orca import Extension, find_funcs, find_nodes
from pathlib import Path

__orca_extension__ = Extension(
    title='demo-orca-ext',
    publisher='robby',
    version='1.0.0',
    nodes=find_nodes(Path(__file__).parent / 'nodes'),
    funcs=find_funcs(Path(__file__).parent / 'funcs')
)