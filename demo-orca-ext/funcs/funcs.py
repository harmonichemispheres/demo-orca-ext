from orca import orca_fn

@orca_fn()
def log(msg):
    print(msg)
    return msg